# Dockerfiles to use Linux commands in a Gitlab Runner 
curl, helm and kubectl running on top of alpline for use with kubernetes clusters.

### Use inside Gitlab CI for auto deployment to kubernetes clusters

```
CURL_IMAGE: registry.gitlab.com/dishantsethi/dockerfiles/curl:latest                                    
KUBECTL_IMAGE: registry.gitlab.com/dishantsethi/dockerfiles/kubectl:latest                                       
HELM_IMAGE: registry.gitlab.com/dishantsethi/dockerfiles/helm:latest  
```                                      

## Gitlab CI Example
```
deploy-to-kubernetes:
    image:
        name: $HELM_IMAGE
```

# Run CURL_IMAGE
```
docker pull registry.gitlab.com/dishantsethi/dockerfiles/curl:latest
docker run --rm registry.gitlab.com/dishantsethi/dockerfiles/curl:latest
```

# Run KUBECTL_IMAGE
```
docker pull registry.gitlab.com/dishantsethi/dockerfiles/kubectl:latest
docker run --rm registry.gitlab.com/dishantsethi/dockerfiles/kubectl:latest
```

# Run HELM_IMAGE
```
docker pull registry.gitlab.com/dishantsethi/dockerfiles/helm:latest
docker run --rm registry.gitlab.com/dishantsethi/dockerfiles/helm:latest
```

